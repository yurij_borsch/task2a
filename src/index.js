import express from 'express';
import fetch from 'isomorphic-fetch';
import cors from 'cors';


const app = express();
app.use(cors());

app.get('/task2A', (req, res) => {
	console.log(req.query);
	if (req.query.a !== undefined) {
		var a = +req.query.a;
	} else {
		var a = 0;
	}
	
	if (req.query.b !== undefined) {
		var b = +req.query.b;
	} else {
		var b = 0;
  }
	const sum = a + b;
	//console.log('sum=' + sum);
	res.send(sum + '');
})

app.get('/task2B', (req, res) => {
	console.log(req.query);
	if (req.query.fullname && req.query.fullname.search(/[\d_\\\/]/) < 0) {
		const fullname = req.query.fullname.trim().toLowerCase();
		const fio_array = fullname.split(/\s+/i); // ���� �� ����� �� �������
		const len_ar = fio_array.length;
		//console.log(fio_array + ' ' + len_ar);
		for(var i=0; i<len_ar; i++) {
			const len_el = fio_array[i].length - 1;
			fio_array[i] = fio_array[i][0].toUpperCase() + fio_array[i].slice(-len_el);
			//console.log(fio_array[i]);
		}
		switch(len_ar) {
			case 1: res.send(fio_array[0]);
			case 2: res.send(fio_array[1] + ' ' + fio_array[0][0] + '.');
			case 3: res.send(fio_array[2] + 
				' ' + fio_array[0][0] + '.' + 
				' ' + fio_array[1][0] + '.'); 
				break;
			default: res.send('Invalid fullname');
		}
	} else {
		res.send('Invalid fullname');
	}
})

app.get('/task2C', (req, res) => {
	console.log(req.query);
	const re = /(https?:\/\/)?((\w+(--|\.))+(\w+\/))?@?(\w+\.?\w+)/i;
	const un = req.query.username.match(re);
	const user = un[un.length - 1];
	//console.log(un);
	res.send('@' + user);
})
	
app.listen(3000, function() {
	console.log("App listening on port 3000!");
})


